package com.zebra.bussiness.service.impl.extend;

import com.zebra.bussiness.domain.CommodityInfo;
import com.zebra.bussiness.mapper.CommodityInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class CommodityOrderServiceImplExtend extends BaseServiceImplExtend {
    @Autowired
    private CommodityInfoMapper commodityInfoMapper;

    protected String getCategoryName(String commodityId) {
        CommodityInfo commodityInfo = commodityInfoMapper.selectByPrimaryKey(commodityId);
        if (commodityInfo != null)
            return commodityInfo.getCommodityName();
        return null;
    }
}
